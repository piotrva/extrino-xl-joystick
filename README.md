# README #
USB HID Joystick: 7 axis, 2 hat switches, 32 buttons
Used: 5 axis, 1 hat switch, 9 buttons (4 as resistive ladder)

### Hardware ###
* Logitech Wingman Warrior (with custom electronics)
* eXtrino XL [Leon Instruments](http://www.leon-instruments.pl/2014/05/extrino-xl.html)

### Source of information ###
* [HID Joystick using XMega](http://www.avrfreaks.net/forum/custom-generic-usb-hid-device-not-reconized)
* [Joystick using ATMega32U4 @ Arduino Leonardo](http://www.imaginaryindustries.com/blog/?p=80)

### How do I get set up? ###

* See documentation of eXtrino XL
* See /EXTRINO_JOY/src/Hardware/Hardware.h for ADC and button configuration/connection
* See /EXTRINO_JOY/src/main.c for additional modifications