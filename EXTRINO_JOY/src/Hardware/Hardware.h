/*
 * Hardware.h
 *
 * Created: 2015-08-05 00:23:26
 *  Author: Piotr
 */ 


#ifndef HARDWARE_H_
#define HARDWARE_H_

#include <asf.h>

#define MY_ADC    ADCA
#define MY_ADC_CH ADC_CH0
#define NEG_INPUT ADCCH_NEG_PAD_GND

#define MY2_ADC    ADCB
#define MY2_ADC_CH ADC_CH0

#define USART_SERIAL                     &USARTD0
#define USART_SERIAL_BAUDRATE            9600
#define USART_SERIAL_CHAR_LENGTH         USART_CHSIZE_8BIT_gc
#define USART_SERIAL_PARITY              USART_PMODE_DISABLED_gc
#define USART_SERIAL_STOP_BIT            false
#define USART_TXD                        IOPORT_CREATE_PIN(PORTD, 3)

//B3
#define X_ADC_CH ADCCH_POS_PIN3
#define X_ADC &ADCB
//B1
#define Y_ADC_CH ADCCH_POS_PIN1
#define Y_ADC &ADCB
//B2
#define TH_ADC_CH ADCCH_POS_PIN2
#define TH_ADC &ADCB
//B0
#define BTN_ADC_CH ADCCH_POS_PIN0
#define BTN_ADC &ADCB
//A1
#define XR_ADC_CH ADCCH_POS_PIN1
#define XR_ADC &ADCA
//A2
#define YR_ADC_CH ADCCH_POS_PIN2
#define YR_ADC &ADCA

//A5
#define PL_ADC_CH ADCCH_POS_PIN5
#define PL_ADC &ADCA
//A6
#define PR_ADC_CH ADCCH_POS_PIN6
#define PR_ADC &ADCA

#define BTN1 IOPORT_CREATE_PIN(PORTD, 5)
#define BTN2 IOPORT_CREATE_PIN(PORTC, 2)
#define BTN3 IOPORT_CREATE_PIN(PORTD, 4)
#define BTN4 IOPORT_CREATE_PIN(PORTC, 3)
#define BTN9 IOPORT_CREATE_PIN(PORTC, 4)

void adc_init(void);
void serial_init(void);

uint16_t  adc_get_unsigned(ADC_t* adc, enum adcch_positive_input input);



#endif /* HARDWARE_H_ */