/*
 * Hardware.c
 *
 * Created: 2015-08-05 00:23:12
 *  Author: Piotr
 */ 
#include "Hardware.h"

void adc_init(void){
	struct adc_config adc_conf;
	struct adc_channel_config adcch_conf;
	adc_read_configuration(&MY_ADC, &adc_conf);
	adcch_read_configuration(&MY_ADC, MY_ADC_CH, &adcch_conf);
	adc_set_conversion_parameters(&adc_conf, ADC_SIGN_ON, ADC_RES_12,
	ADC_REF_VCCDIV2);
	adc_set_conversion_trigger(&adc_conf, ADC_TRIG_MANUAL, 1, 0);
	adc_set_clock_rate(&adc_conf, 200000UL);
	adcch_set_input(&adcch_conf, ADCCH_POS_PIN1, NEG_INPUT, 0);
	adc_write_configuration(&MY_ADC, &adc_conf);
	adcch_write_configuration(&MY_ADC, MY_ADC_CH, &adcch_conf);
	adc_enable(&MY_ADC);
	
	adc_read_configuration(&MY2_ADC, &adc_conf);
	adcch_read_configuration(&MY2_ADC, MY2_ADC_CH, &adcch_conf);
	adc_set_conversion_parameters(&adc_conf, ADC_SIGN_ON, ADC_RES_12,
	ADC_REF_VCCDIV2);
	adc_set_conversion_trigger(&adc_conf, ADC_TRIG_MANUAL, 1, 0);
	adc_set_clock_rate(&adc_conf, 200000UL);
	adcch_set_input(&adcch_conf, ADCCH_POS_PIN1, NEG_INPUT, 0);
	adc_write_configuration(&MY2_ADC, &adc_conf);
	adcch_write_configuration(&MY2_ADC, MY2_ADC_CH, &adcch_conf);
	adc_enable(&MY2_ADC);
}

void serial_init(void){
	// Set the TxD pin high - set PORTC DIR register bit 3 to 1
	//PORTD.OUTSET = PIN3_bm;
	ioport_set_pin_level(USART_TXD, 1);
	
	// Set the TxD pin as an output - set PORTC OUT register bit 3 to 1
	//PORTD.DIRSET = PIN3_bm;
	ioport_set_pin_dir(USART_TXD, IOPORT_DIR_OUTPUT);
	static usart_serial_options_t usart_options = {
		.baudrate = USART_SERIAL_BAUDRATE,
		.charlength = USART_SERIAL_CHAR_LENGTH,
		.paritytype = USART_SERIAL_PARITY,
		.stopbits = USART_SERIAL_STOP_BIT
	};
	
	usart_serial_init(USART_SERIAL, &usart_options);
}

uint16_t adc_get_unsigned(ADC_t* adc, enum adcch_positive_input input){
	struct adc_channel_config adcch_conf;
	
	adcch_read_configuration(adc, MY_ADC_CH, &adcch_conf);
	adcch_set_input(&adcch_conf, input, NEG_INPUT, 0);
	adcch_write_configuration(adc, MY_ADC_CH, &adcch_conf);
	
	adc_start_conversion(adc, MY_ADC_CH);
	adc_wait_for_interrupt_flag(adc, MY_ADC_CH);
	int16_t result = adc_get_result(adc, MY_ADC_CH);
	if(result < 0) result = 0;
	return (uint16_t)result;
}