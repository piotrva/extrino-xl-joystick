/**
 * \file main.c
 *
 * \brief main file for joystick usb driver
 * \author Piotr Rzeszut
 *
 */
#include <asf.h>
#include <string.h>
#include <stdio.h>

#include "Joystick/joystick.h"
#include "Hardware/Hardware.h"

volatile JoyState_t joystick;
volatile JoyState_t joystick2;
volatile uint8_t mscounter = 0;

char bufor[20];
int32_t map(int32_t x, int32_t in_min, int32_t in_max, int32_t out_min, int32_t out_max);

int32_t map(int32_t x, int32_t in_min, int32_t in_max, int32_t out_min, int32_t out_max)
{
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void int1ms(void);

void int1ms(void){
	if(joystick.data.buttons&((1UL<<31)|(1UL<<30))&&mscounter<=10){
		joystick.data.buttons &= ~((1UL<<30)|(1UL<<31));
	}
	if(mscounter)mscounter--;
}

int main (void)
{
	
	sysclk_init();
	irq_initialize_vectors();
	cpu_irq_enable();
	
	pmic_init();
	tc_enable(&TCC1);
	tc_set_overflow_interrupt_callback(&TCC1, int1ms);
	tc_set_wgm(&TCC1, TC_WG_NORMAL);
	tc_write_period(&TCC1, 24000);
	tc_set_overflow_interrupt_level(&TCC1, TC_INT_LVL_LO);
	//cpu_irq_enable();
	tc_write_clock_source(&TCC1, TC_CLKSEL_DIV1_gc);
	
	board_init();
	
	udc_start();
	user_callback_vbus_action(true);
	
	adc_init();
	
	serial_init();
	
	qdec_config_t config;
	qdec_get_config_defaults(&config);
	qdec_config_phase_pins(&config, &PORTD, 0, false, 500);
	qdec_config_revolution(&config, 120);
	qdec_config_disable_index_pin(&config);
	qdec_enabled(&config);
	
	ioport_set_pin_dir(BTN1, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(BTN1, IOPORT_MODE_PULLUP);
	
	ioport_set_pin_dir(BTN2, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(BTN2, IOPORT_MODE_PULLUP);
	
	ioport_set_pin_dir(BTN3, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(BTN3, IOPORT_MODE_PULLUP);
	
	ioport_set_pin_dir(BTN4, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(BTN4, IOPORT_MODE_PULLUP);
	
	ioport_set_pin_dir(BTN9, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(BTN9, IOPORT_MODE_PULLUP);
	
	uint16_t rotary_old = 0;
	uint16_t pl_min=0xFFFF, pr_min=0xFFFF;
	uint16_t pl_max=0, pr_max=0;
	
	delay_ms(100);
	
	while(1) {
		joystick.data.report_id = 0x01;
		//joystick.data.xAxis=127;
		//joystick.data.yAxis=127;
		//joystick.data.zAxis=127;
		
		//joystick.data.xRotAxis=127;
		//joystick.data.yRotAxis=127;
		joystick.data.zRotAxis=127;
		
		joystick.data.hatSw1 = 0;
		joystick.data.hatSw2 = 0;
		
		//joystick.data.rudder = 127;
		//joystick.data.throttle = 127;
		
		//joystick.data.buttons<<=1;
		//if(joystick.data.buttons == 0) joystick.data.buttons = 1;
		
		joystick2.data.report_id = 0x03;
		joystick2.data.xAxis=127;
		joystick2.data.yAxis=127;
		joystick2.data.zAxis=127;
		
		joystick2.data.xRotAxis=127;
		joystick2.data.yRotAxis=127;
		joystick2.data.zRotAxis=127;
		
		joystick2.data.hatSw1 = 0;
		joystick2.data.hatSw2 = 0;
		
		//joystick2.data.rudder = 127;
		joystick2.data.throttle = 127;
		joystick2.data.buttons = 0;
		
		joystick.data.xAxis = 255-adc_get_unsigned(X_ADC, X_ADC_CH)/8;
		joystick.data.yAxis = 255-adc_get_unsigned(Y_ADC, Y_ADC_CH)/8;
		joystick.data.throttle = adc_get_unsigned(TH_ADC, TH_ADC_CH)/8;
		joystick.data.xRotAxis = 255-adc_get_unsigned(XR_ADC, XR_ADC_CH)/8;
		joystick.data.yRotAxis = 255-adc_get_unsigned(YR_ADC, YR_ADC_CH)/8;
		
		uint16_t pl=adc_get_unsigned(PL_ADC, PL_ADC_CH)/8;
		uint16_t pr=adc_get_unsigned(PR_ADC, PR_ADC_CH)/8;
		
		if(pl>pl_max)pl_max=pl;
		if(pr>pr_max)pr_max=pr;
		if(pl<pl_min)pl_min=pl;
		if(pr<pr_min)pr_min=pr;
		
		pl = map(pl,pl_min,pl_max,0,255);
		pr = map(pr,pr_min,pr_max,0,255);
		
		uint16_t rudder = 127 + pr/2 -pl/2;
		
		joystick2.data.rudder = rudder;
		
		int32_t l_brk = min(pl,pr) + map(rudder,0,255,-(int32_t)min(pl,pr),min(pl,pr));
		int32_t r_brk = min(pl,pr) - map(rudder,0,255,-(int32_t)min(pl,pr),min(pl,pr));
		
		joystick2.data.xAxis = l_brk;
		joystick2.data.yAxis = r_brk;
		
		
		
		joystick.data.buttons &= ~0b00111111111111111111111111111111;
		
		uint16_t way4 = adc_get_unsigned(BTN_ADC, BTN_ADC_CH);
		if(way4 > 1250){
			//none
		}else if(way4 > 1092){
			joystick.data.buttons |= 1<<4;
		}else if(way4 > 822){
			joystick.data.buttons |= 1<<5;
		}else if(way4 > 322){
			joystick.data.buttons |= 1<<6;
		}else{
			joystick.data.buttons |= 1<<7;
		}
		
		if(ioport_get_pin_level(BTN1) == 0) joystick.data.buttons |= 1<<0;
		if(ioport_get_pin_level(BTN2) == 0) joystick.data.buttons |= 1<<1;
		if(ioport_get_pin_level(BTN3) == 0) joystick.data.buttons |= 1<<2;
		if(ioport_get_pin_level(BTN4) == 0) joystick.data.buttons |= 1<<3;
		if(ioport_get_pin_level(BTN9) == 0) joystick.data.buttons |= 1<<8;
		
		uint16_t rotary = qdec_get_position(&config);
		
		//joystick.data.hatSw1 = 7-rotary/15;
		
		if(rotary != rotary_old){
			rotary_old = rotary;
			//if(rotary%5 == 0){
				if(qdec_get_direction(&config)){
					if(mscounter==0){
						joystick.data.buttons |= (1UL<<31);
						joystick.data.buttons &= ~(1UL<<30);
						mscounter=20;
					}
					}else{
					if(mscounter==0){
						joystick.data.buttons |= (1UL<<30);
						joystick.data.buttons &= ~(1UL<<31);
						mscounter=20;
					}
				}
			//}
		}
		
		/*sprintf(bufor, "%u\r\n", qdec_get_position(&config));
		usart_serial_write_packet(USART_SERIAL, (uint8_t*)bufor, strlen(bufor));*/
		delay_ms(1);
		joystickSetState((JoyState_t*)&joystick);
		delay_ms(1);
		joystickSetState((JoyState_t*)&joystick2);
		//delay_ms(250);
		
	}
}
