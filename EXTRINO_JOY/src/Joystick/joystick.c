/*
 * joystick.c
 *
 * Created: 2015-08-03 21:00:06
 *  Author: Piotr
 */ 
#include "joystick.h"

static bool my_flag_autorize_generic_events = false;
bool my_callback_generic_enable(void)
{
	my_flag_autorize_generic_events = true;
	return true;
}
void my_callback_generic_disable(void)
{
	my_flag_autorize_generic_events = false;
}

void my_callback_generic_report_out(uint8_t *report)
{

}

void my_callback_generic_set_feature(uint8_t *report_feature)
{

}

void user_callback_vbus_action(bool b_vbus_high){
	if(b_vbus_high){
		udc_attach();
		}else{
		udc_detach();
	}
}

void joystickSetState(JoyState_t* state){
	if (!my_flag_autorize_generic_events) {
		return;
	}
	udi_hid_generic_send_report_in(state->raw);
}

