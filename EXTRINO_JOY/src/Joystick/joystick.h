/*
 * joystick.h
 *
 * Created: 2015-08-03 21:00:19
 *  Author: Piotr
 */ 


#ifndef JOYSTICK_H_
#define JOYSTICK_H_

#include <asf.h>

typedef struct{
	uint8_t		report_id;
	uint32_t	buttons;
	
	uint8_t		throttle;
	uint8_t		rudder;
	
	uint8_t		hatSw1:4;
	uint8_t		hatSw2:4;
	
	uint8_t		xAxis;
	uint8_t		yAxis;
	uint8_t		zAxis;

	uint8_t		xRotAxis;
	uint8_t		yRotAxis;
	uint8_t		zRotAxis;

	} JoyStateInt_t;

typedef union{
	JoyStateInt_t data;
	uint8_t raw[14];
	}JoyState_t;

bool my_callback_generic_enable(void);
void my_callback_generic_disable(void);

void my_callback_generic_report_out(uint8_t *report);

void my_callback_generic_set_feature(uint8_t *report_feature);

void user_callback_vbus_action(bool b_vbus_high);

void joystickSetState(JoyState_t* state);

#endif /* JOYSTICK_H_ */
